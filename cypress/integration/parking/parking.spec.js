describe('parking feature', () => {
    it('should display the correct homepage title', () => {
        cy.visit('http://www.shino.de/parkcalc/index.php')
        cy.title().should('eq','Parking Cost Calculator')
    });
    it(' Short-Term Parking cost calculation', () => {
        cy.get('#ParkingLot').select('Short-Term Parking')
        cy.get('#ParkingLot').should('have.value' , 'Short')
        cy.get('input#StartingDate').clear().type('8/3/2020')
        cy.get('input#LeavingDate').clear().type('8/3/2020')
        cy.get('input#LeavingTime').clear().type('14:00')
        // cy.wait(3000)
        cy.get('[type="submit"]').click()
        cy.get('.SubHead > b').should('have.text', '$ 24.00')
    });
})
